import { Component, HostBinding, OnInit, HostListener } from '@angular/core';
import { Router, Event, RouterOutlet, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { slideRight } from './animations';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { ThankuComponent } from './components/thanku/thanku.component';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],

})

export class AppComponent implements OnInit {
  hide: boolean;

  contactForm: FormGroup;
  public show: boolean = false;
  public submenu: boolean = false;
  whatwedoline: string;
  technologyline: string;
  aboutline: string;
  traningline: string;
  hiringline: string;
  progressBar = false;
  menu = "../assets/images/menu.png"
  close = "../assets/images/cross.png"
  click: any;
  btn1: any;
  name: string;
  email: string;
  mobileNo: string
  queryFor: string;
  message: string;
  showErrors: boolean;

  appDesign = "App Design";
  webDevelopment = "Web Development"
  appDevelopment = "App Development";
  bidding = "Bidding";
  androidAppDevelopment = "Android App Development";
  iosAppDevelopment = "IOS App Development";
  public iconName: any = this.menu;
  isScroll = true;
  apiUrl = "http://18.139.111.3:6300/api/contactUs"

  routeUrl: string;
  isTopButton: boolean = false;
  a: number;
  b: string = null;
  c: boolean = undefined;

  constructor(private router: Router, private http: HttpClient, public dialog: MatDialog) {



    window.onscroll = () => {
      this.scrollFunction2();

    }

    this.router.events.subscribe((event: Event) => {

      if (event instanceof NavigationEnd) {
        this.routeUrl = this.router.url;
        if (this.routeUrl != '/home') {
          this.isScroll = false;
          document.getElementById("navbar").style.opacity = "1";
        } else {
          this.isScroll = true;
        }
      }
    });

    this.router.events.subscribe((value: Event) => {
      if (value instanceof NavigationEnd) {
        if (value.url == "/technologies") {
          this.technologyline = "line"
          this.whatwedoline = "";
          this.aboutline = "";
          this.hiringline = "";
          this.traningline = "";
        } else if (value.url == "/about") {
          this.whatwedoline = "";
          this.technologyline = "";
          this.hiringline = "";
          this.aboutline = "line";
          this.traningline = "";
        } else if (value.url == "/training") {
          this.whatwedoline = "";
          this.hiringline = "";
          this.technologyline = "";
          this.aboutline = "";
          this.traningline = "line";
        } else if (value.url == "/hiring") {
          this.whatwedoline = "";
          this.technologyline = "";
          this.aboutline = "";
          this.traningline = "";
          this.hiringline = "line";
        } else {
          this.hiringline = "";
          this.whatwedoline = "";
          this.technologyline = "";
          this.aboutline = "";
          this.traningline = "";
        }
      }
      window.scrollTo(0, 0);
    })
  }

  ngOnInit() {
    this.whatwedoline = "";
    this.technologyline = "";
    this.aboutline = "";
    this.traningline = "";
    this.hiringline = "";
    this.name = "";
    this.email = "";
    this.message = "";
    this.mobileNo = "";
    this.queryFor = "";
    this.showErrors = false;

    this.contactForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      mobile: new FormControl(null, Validators.required),
      email: new FormControl(null, Validators.required),
      query: new FormControl(null, Validators.required),
      message: new FormControl(null, Validators.required),
    })
  }


  toggle() {
    this.show = !this.show;
    if (this.show) {
      this.iconName = this.close;
    } else {
      this.iconName = this.menu;
    }

  }

  msubdroupdown() {
    this.submenu = !this.submenu;
  }


  top1() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });

  }

  scrollFunction2() {

    if (this.isScroll) {
      const slider = document.getElementById("slider");
      if (slider) {
        if (document.documentElement.scrollTop > slider.clientHeight - 70) {
          document.getElementById("navbar").style.opacity = "1";
        }
        else {
          document.getElementById("navbar").style.opacity = "0.7";
        }
      }
    }
    if (document.documentElement.scrollTop > 700) {
      this.isTopButton = true;
    }
    else {
      this.isTopButton = false;

    }
  }

  showlist() {
    // a.style.display="block"
    document.getElementById("list-hide").style.display = "block";
  }
  hidelist() {
    document.getElementById("list-hide").style.display = "none";
  }



  navroute(value) {
    document.getElementById("list-hide").style.display = "none";
    this.router.navigate([value]);
    if (this.show) {
      this.toggle();
    }

  }
  navsubroute(value, id) {
    document.getElementById("list-hide").style.display = "none";
    this.router.navigate([value, id]);
    if (this.show) {
      this.toggle();
    }
  }
  letstalk() {
    document.getElementById("closebtn1").style.display = "block";
    document.getElementById("mask1").style.bottom = "0%";
  }
  closemask() {
    document.getElementById("mask1").style.bottom = "100%";
    document.getElementById("closebtn1").style.display = "none";
  }



  // <--------------------------------start------------------------------------------------->





  // onKey(value, id) {
  //   switch (id) {
  //     case 0:
  //       this.name = value.target.value;
  //       break;

  //     case 1:
  //       this.email = value.target.value;
  //       break;

  //     case 2:
  //       this.mobileNo = value.target.value;
  //       break;

  //     case 3:
  //       this.message = value.target.value;
  //       break;
  //     default:
  //       break;
  //   }
  // }

  onSubmit1(event) {
    if (this.name == "") {
      alert('error name')
      this.showErrors = true;
    }
    else if (this.email == "") {
      this.showErrors = true;
      alert('error email')



    }
    else if (this.mobileNo == "") {
      alert('error mobile')
      this.showErrors = true;


    }
    else if (this.queryFor == "") {
      this.showErrors = true;
      alert('error quuq')


    } else if (this.message == "") {
      this.showErrors = true;
      alert('error mess')

    }
    else {

      var body = {
        name: this.name,
        email: this.email,
        phone: this.mobileNo,
        queryFor: this.queryFor,
        message: this.message
      }
      this.http.post(this.apiUrl, body).subscribe((res: any) => {


        if (res != undefined && res != null) {
          if (res.isSuccess) {
            this.progressBar = false
            this.openDialog();
          }
        }
      }, error => {
        console.log(error);
        this.progressBar = false;
        alert(error.error.error);

      })


    }
  }

  postData() {

  }
  openDialog() {
    const dialogRef = this.dialog.open(ThankuComponent, {
      panelClass: 'my-class'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  onNameKey(event) {
    this.name = event.target.value
  }
  onEmailKey(event) {
    this.email = event.target.value
  }
  onMobileKey(event) {
    this.mobileNo = event.target.value
  }
  onQueryChange(event) {
    this.queryFor = event.value
  }
  onMessageKey(event) {
    this.message = event.target.value
  }


  onProfileFormSubmit() {

  }

}

// .....................................end...............................................
