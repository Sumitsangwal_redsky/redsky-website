import { Component, OnInit } from '@angular/core';

import { Router, Event, RouterOutlet, NavigationStart, NavigationEnd, ActivatedRoute, } from '@angular/router';

@Component({
  selector: 'app-hybridapp',
  templateUrl: './hybridapp.component.html',
  styleUrls: ['./hybridapp.component.css']
})
export class HybridappComponent implements OnInit {

  screen1;
  screen2;
  screen3;
  id: any;
  isShow = false;
  isNodejs: boolean = true;
  isfigma: boolean = false;

  constructor(private actrouter: ActivatedRoute, private router: Router, ) {

    this.router.events.subscribe((value: Event) => {

      if (value instanceof NavigationEnd) {
        this.actrouter.paramMap.subscribe(res => {
          this.id = res.get('id');
          if (this.id == 0 || this.id == 2) {
            this.isShow = true;
            // document.getElementById('react').style.display = "block";
          } else {
            this.isShow = false;
            // document.getElementById('react').style.display = "none";
          }
          if (this.id > 0) {
            this.isNodejs = false;
          } else {
            this.isNodejs = true;
          }

          if (this.id == 2) {
            this.isfigma = true;
          }else{
            this.isfigma = false;

          }
        });


      }
    })
  }

  ngOnInit() {
    this.screen1 = [
      {
        id: 0, heading: 'Create Native iOS and Android Apps with JavaScript', p: `Open source framework for building truly native mobile apps with Angular, Vue.js, TypeScript, or
      JavaScript.`, img: '../../../assets/images/nativescript-background.png', react: [{
          id: 0, heading: 'React Native', p: `React Native lets you build mobile apps using only JavaScript. It uses the same design as React, letting you compose a rich mobile UI from declarative components.`,
          img: '../../../assets/images/reactnative-background.png'
        }]
      },
      {
        id: 1, heading: 'One Framework Mobile & desktop', p: `Learn one way to build applications with Angular and reuse your code and abilities to build apps for any deployment target`,
        img: '../../../assets/images/angular-background.png'
      },
      {
        id: 2, heading: 'Adobe XD', p: `XD brings design and prototyping together with fast, intuitive tools that get you into your designs and get out of your way`,
        img: '../../../assets/images/adobe-background.png', react: [{
          id: 0, heading: 'Gravit Designer', p: `Whether you’re working on graphics for marketing materials, icons, UI design and flexibility you need to unleash your creativity`,
          img: '../../../assets/images/gravity-background.png'
        }],
        figma: [{
          id: 0, heading: 'Figma Designer', p: `Packed with design features you already love plus unique inventions like the Arc tool and Vector Networks, Figma helps you keep the ideas flowing`,
          img: '../../../assets/images/figma-background.png'
        }]
      },
      {
        id: 3, heading: 'Node.js', p: `As an asynchronous event-driven JavaScript runtime, Node.js is designed to build scalable network applications`,
        img: '../../../assets/images/nodejs-background.png'
      },
    ]





    this.screen2 = [
      {
        id: 0, heading: 'Why you need NativeScript', card: [{
          id: 0, heading: 'Native Performance', p: `Beautiful, accessible, platform-native UI - without WebViews. Define once and let NativeScript adapt
        to run everywhere, or tailor the UI to specific devices and screens. This is one of the reasons SAP
        chose NativeScript.` },
        {
          id: 1, heading: 'Free and Open Source', p: `NativeScript is 100% free and open source, Apache 2 licensed.`
        }, {
          id: 2, heading: 'Cross-Platform', p: `Write and deploy native mobile apps for iOS and Android from a single codebase. Use Angular or Vue to
          share existing web-based code.`
        }],
        react: [{
          id: 0, heading: 'Why you need React Native', card: [{
            id: 0, heading: 'Strong Community', p: `React Native can open doors to great community support in terms of cross-platform mobile app development. Soon after React Native was launched in 2015 Facebook, it became an instant favorite among the developers worldwide.` },
          {
            id: 1, heading: 'Development Time', p: `The time used by React Native to develop a mobile app is much lesser than other app development frameworks. That’s why React Native For Hybrid App Development is a picture perfect choice. It will help you to save time as well as the money.`
          }, {
            id: 2, heading: 'Cross-Platform', p: `Write and deploy native mobile apps for iOS and Android from a single codebase. Use Angular or Vue to
            share existing web-based code.`
          }],

        }]

      },

      {
        id: 1, heading: 'Why you need Angular', card: [{
          id: 0, heading: 'CROSS PLATFORM', p: `Use modern web platform capabilities to deliver app-like experiences. High performance, and zero-step installation.
          Create apps across Mac, Windows, and Linux using the same Angular`
        }, {
          id: 1, heading: 'PERFORMANCE', p: `Angular turns your templates into code that's highly optimized for today's JavaScript virtual machines, giving you all the benefits of hand-written code with the productivity of a framework.`
        }, {
          id: 2, heading: 'PRODUCTIVITY', p: `Command line tools: start building fast, add components and tests, then instantly deploy. 
          Quickly create UI views with simple and powerful template syntax.`
        },]
      },
      {
        id: 2, heading: 'Why you need Adobe XD', card: [{
          id: 0, heading: 'Responsive Size', p: `Easily resize groups of objects or components for different size screens and maintain relative placement and scalability. Make quick manual adjustments to customize.`
        }, {
          id: 1, heading: 'Precise design', p: `Lay out and align your experience with artboard guides, layout grids, nudge and resize shortcuts and relative measurement features.`
        }, {
          id: 2, heading: 'Optimised Tools', p: `Work with a focused set of drawing and shape tools, Boolean operators, blend modes and vector editing features that are optimised for creating wireframes, icons and other visual elements.`
        }
        
        // , {
        //   id: 3, heading: 'Assets panel', p: `Make colors and character styles easily available for reuse by adding them to the Assets panel, which automatically includes components. Edit any color or character style in the panel, and the changes update throughout your document.`
        // }, {
        //   id: 4, heading: 'Repeat Grid', p: `Select an item in your design, such as a contact list or photo gallery, and replicate it as many times as you want — all your styles and spacing stay intact. Update an element once, and your changes update everywhere.`
        // }, {
        //   id: 5, heading: 'Batch asset export', p: `Quickly mark elements for export, choose to batch export them for your target platform or include them in design specs for developers to download.`
        // },
        ],
        react: [{
          id: 0, heading: 'Why you need Gravit Designer', card: [{
            id: 0, heading: 'Web and App UI​', p: `Find everything you need for UI Design. Create mock ups and wireframes for web and mobile using shared styles, symbols, overrides, anchors, and master pages`
          },
          {
            id: 1, heading: 'Image and Photo Editing', p: `From color and lighting adjustments, to filters and blending, Designer has the photo effects you are looking for to edit images for posting, print, or as part of a larger design project.`
          }, {
            id: 2, heading: 'Icons, Branding, Logos', p: `Create professional, buzz worthy brand visuals and imagery that make a statement. Gravit Designer PRO gives you so many options to create stand-out projects.`
          }],

        }],
        figma: [{
          id: 0, heading: 'Why you need Figma Designer', card: [{
            id: 0, heading: 'Design and prototype in tandem', p: `Bring your ideas to life faster in animated prototypes that feel like the real thing. Get insights from users and test concepts earlier and more often.`
          },
          {
            id: 1, heading: 'Team up to move even faster', p: `Share a link to your design files or prototypes, and get feedback in context. Or, jump into the same file with your teammates no matter where y’all are in the world and co-edit live.`
          }, {
            id: 2, heading: 'Build once, reuse infinite times', p: `Create a scalable design system that’s accessible for your organization and easy for you to manage. When all designers are speaking the same language, everyone’s more empowered to do their best work.`
          }],

        }]
      },
      {
        id: 1, heading: 'Why you need NodeJS', card: [{
          id: 0, heading: 'PERFORMANCE', p: `Node.js is built on Google Chrome’s V8 JavaScript engine, which helps us in faster code execution. The engine compiles the JavaScript code into machine code which makes our code easier and faster to implement in an effective manner. Node.js uses events highly which makes it pretty fast.`
        }, {
          id: 1, heading: 'UNIFIED API', p: `Node.js can be combined with a browser, a database that supports JSON data (such as Postgres,[74] MongoDB, or CouchDB) and JSON for a unified JavaScript development stack. With the adaptation of what were essentially server-side development patterns such as MVC, MVP, MVVM, etc.`
        }, {
          id: 2, heading: 'EVENT LOOP', p: `Node.js registers with the operating system so the OS notifies it of connections and issues a callback. Within the Node.js runtime, each connection is a small heap allocation. Traditionally, relatively heavyweight OS processes or threads handled each connection. Node.js uses an event loop for scalability, instead of processes or threads.`
        },]
      },
    ]

    this.screen3 = [
      { id: 0, heading: 'How it works', img: '../../../assets/images/how-it-works-min.png', react: [{ heading: 'How it works', img: '../../../assets/images/reactnative.png' }] },
      // { id: 1, heading: 'How it works', img: '../../../assets/images/angularframe1.png' },
      // { id: 2, heading: 'How it works', img: '../../../assets/images/angularframe1.png', react: [{ heading: 'How it works', img: '../../../assets/images/designer-image.jpg' }] }

    ]


  }

}
