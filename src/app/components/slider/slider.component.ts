import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { trigger, transition, state, animate, style } from '@angular/animations';


@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css'],
  providers: [NgbCarouselConfig],
  animations: [trigger("fade", [
    state("void", style({ opacity: 0 })),
    transition("void <=> *", [animate("0.5s ease-in-out")])
  ]),


  [trigger("line1", [
    state("void", style({  opacity: 0 , padding: 0, transform: 'translate(0px,0px)'})),
    state("*", style({
      opacity: 1,
      // margin: 0, padding: 0,
      transform: 'translate(0px,0px)'
      
    })),
    transition("void => *", [animate("0.5s")]),
    
  ])],

  [trigger("p", [
    state("void", style({ transform: 'translate(5%, -50px)', margin: 0, padding: 0 })),
    state("*", style({
      opacity: 1,
      transform: 'translate(0px,0px)'
    })),
    transition("void => *", [animate("0.4s ease-in")]),
    transition(':leave', [
      animate('0.4s', style({ opacity: 0, transform: 'translate(10%, 10%)', margin: 0, padding: 0 }))
    ])
  ])],


  ]

})
export class SliderComponent implements OnInit {

  showNavigationArrows = false;
  showNavigationIndicators = false;
  images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);

  counter = 0;
  counter2 = 0;
  line1 = true;
  interval;

  slideItems = [
    { p1: 'We Create', p2: 'Stunning', p3: 'Websites' },
    { p1: 'We Develop', p2: 'Scalable', p3: 'Mobile Apps' },
    { p1: 'We Design', p2: 'Unique', p3: 'Brand Identities' },
  
  ];
  slideItems2 = [
    { src: '../assets/images/android.png', title: 'Web Development', p: 'RedSky Advance Technology brings together creative and dynamic web development tools to help you enter the world of web technologies.' },
    { src: '../assets/images/nativ.png', title: 'Mobile App Development', p: 'You can improve your business performance remarkably by investing in a mobile app. In the area of App Development' },
    { src: '../assets/images/angular.png', title: 'Designing', p: 'Want to maximize your conversion rates and engage users? Deftsoft has made it possible for you' }
  ];

  
  nextImg() {
    if (this.counter == this.slideItems.length - 1) {
      this.counter = 0;

    } else {
      if (this.counter < this.slideItems.length) {
        this.counter += 1;
      }
    }
  }

  


  constructor(config: NgbCarouselConfig) {
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;

  }

  ngOnInit() {

    

    this.interval = setInterval(() => {
      setInterval(() => {

        this.nextImg();
        
  
      }, 4000);
      this.pauseTimer();
    },750)

    setInterval(() => {
      if (this.line1) {
        this.line1 = false;

      } else {
        this.line1 = true;

      }

    }, 2000);

    this.nextImg();
  }

  
  
  pauseTimer() {
    
    clearInterval(this.interval);
  }

}
