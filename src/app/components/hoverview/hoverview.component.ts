import { Component, OnInit, AfterViewInit } from '@angular/core';
import { HoverButton } from './HoverButton'
import gsap from "gsap";

@Component({
  selector: 'app-hoverview',
  templateUrl: './hoverview.component.html',
  styleUrls: ['./hoverview.component.css']
})
export class HoverviewComponent implements OnInit, AfterViewInit {

  isHoverAdded: boolean = false;
  btn1Class: any;
  btn2Class: any;
  btn3Class: any;
  btn4Class: any;
  btn5Class: any;
  btn6Class: any;
  btn7Class: any;
  constructor() {

  };


  ngOnInit() {
    window.onscroll = () => {
      if (this.btn1Class && this.btn2Class && this.btn3Class && this.btn4Class && this.btn5Class && this.btn6Class && this.btn7Class) {
        this.btn1Class.calculatePosition();
        this.btn2Class.calculatePosition();
        this.btn3Class.calculatePosition();
        this.btn4Class.calculatePosition();
        this.btn5Class.calculatePosition();
        this.btn6Class.calculatePosition();
        this.btn7Class.calculatePosition();
      }
    }

  }
  ngAfterViewInit(): void {

    this.addEffect()

  }

  async addEffect() {
    const btn1 = document.getElementById('cube1');
    this.btn1Class = await new HoverButton(btn1);
    const btn2 = document.getElementById('cube2');
    this.btn2Class = await new HoverButton(btn2);
    const btn3 = document.getElementById('cube3');
    this.btn3Class = await new HoverButton(btn3);
    const btn4 = document.getElementById('cube4');
    this.btn4Class = await new HoverButton(btn4);
    const btn5 = document.getElementById('cube5');
    this.btn5Class = await new HoverButton(btn5);
    const btn6 = document.getElementById('cube6');
    this.btn6Class = await new HoverButton(btn6);
    const btn7 = document.getElementById('cube7');
    this.btn7Class = await new HoverButton(btn7);

  }



}
