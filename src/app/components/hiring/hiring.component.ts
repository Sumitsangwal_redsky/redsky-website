import { Component, OnInit, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { ThankuComponent } from '../thanku/thanku.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-hiring',
  templateUrl: './hiring.component.html',
  styleUrls: ['./hiring.component.scss']
})
export class HiringComponent implements OnInit {

  posts: any[];
  progressBar = false;
  showErrors: boolean;
  applyForm: FormGroup;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    this.posts = [
      {
        postName: "Nativescript Devloper",
        shortDescription: "Develop Hybrid mobile application using ionic and React.js, Angular JS. Preferably along with Typescript, Nativescript, SQLite. Experience with REST-based APIs and JSON data structures.",
        education: "Bachelors in his field",
        experience: "2 years experience in nativescript",
        jobType: "Full-time",
        schedule: "Day shift",
        skills: "HTML,CSS,JAVASCRIPT,BOOTSTRAP ,Angular2+",
        perkAndBenefits: "5 days a week working, an awesome work environment, and a decent salary hike."
      },
      {
        postName: "Nativescript Devloper",
        shortDescription: "Develop Hybrid mobile application using ionic and React.js, Angular JS. Preferably along with Typescript, Nativescript, SQLite. Experience with REST-based APIs and JSON data structures.",
        education: "Bachelors in his field",
        experience: "2 years experience in nativescript",
        jobType: "Full-time",
        schedule: "Day shift",
        skills: "HTML,CSS,JAVASCRIPT,BOOTSTRAP ,Angular2+",
        perkAndBenefits: "5 days a week working, an awesome work environment, and a decent salary hike."
      },
      {
        postName: "Nativescript Devloper",
        shortDescription: "Develop Hybrid mobile application using ionic and React.js, Angular JS. Preferably along with Typescript, Nativescript, SQLite. Experience with REST-based APIs and JSON data structures.",
        education: "Bachelors in his field",
        experience: "2 years experience in nativescript",
        jobType: "Full-time",
        schedule: "Day shift",
        skills: "HTML,CSS,JAVASCRIPT,BOOTSTRAP ,Angular2+",
        perkAndBenefits: "5 days a week working, an awesome work environment, and a decent salary hike."
      }
    ];

    this.showErrors = false;
    this.applyForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      lastname: new FormControl(null, Validators.nullValidator),
      mobile: new FormControl(null, Validators.required),
      email: new FormControl(null, Validators.required),
      qualification: new FormControl(null, Validators.required),
      university: new FormControl(null, Validators.nullValidator),
    })


  }

  openDialog() {
    const dialogRef = this.dialog.open(ThankuComponent, {
      panelClass: 'my-class'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  showDetails(id) {
    // this.postId = id;
    document.getElementById("closebtn").style.display = "block";
    document.getElementById("mask").style.bottom = "0%";

  }

  closemask() {
    document.getElementById("mask").style.bottom = "100%";
    document.getElementById("closebtn").style.display = "none";
  }

  onSubmit(){

  }

  onKey(args,key){
    
  }

}
