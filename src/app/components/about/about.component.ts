import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, } from '@angular/common/http';
import { Router } from '@angular/router';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { NgxImageGalleryComponent, GALLERY_IMAGE, GALLERY_CONF } from "ngx-image-gallery";
// import { PageEvent } from '@angular/material';
//  import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],
})
export class AboutComponent implements OnInit {
  @ViewChild(NgxImageGalleryComponent) ngxImageGallery: NgxImageGalleryComponent;
  // @ViewChild(NgxUserComponent) ngxUser:NgxUserComponent
  photos: any;
  users:any;
  headers: HttpClient;
  Images: [];
  Users:[]
  userService: any;
  pageNo: number;
  currentPage: number;
  isLoadMore: boolean;
  conf: GALLERY_CONF = {
    imageOffset: '0px',
    showDeleteControl: false,
    showImageTitle: false,
  };


  constructor(private http: HttpClient) {
    this.Images = [];
    this.Users=[];
    this.pageNo = 1;
    this.getImages();
    this.getUsers();
    this.currentPage = 1;
    this.isLoadMore = true;
  }


  ngOnInit() {
    window.scroll(0, 0);

    this.photos = [];
    this.users=[]


  }
  getUsers() {
    // this.Images ;
    this.http.get(`https://redskyatech.com/v1/redsky/apis/Users?pageNo=${this.pageNo}&items=15`)
      .subscribe((res: any) => {
        if (res != null && res != undefined) {
          if (res.isSuccess == true) {
            console.log('Users Res', res);
            if (res.data.length > 0) {  
              this.users=res.data
              // for (var i = 0; i < res.data.length; i++) {

              //   this.users.push({
              //     id: res.data[i]._id,
              //     url: res.data[i].image.url.replace("http://18.140.57.231", "https://redskyatech.com"),
              //     name:res.data[i].name,
              //     description:res.data[i].description

              //   })
              // }
             


            } else {
              this.isLoadMore = false;
            }
          }
        }
      }, error => {
        console.log(error);

      })

  }



  getImages() {
    // this.Images ;
    this.http.get(`https://redskyatech.com/v1/redsky/apis/files?pageNo=${this.pageNo}&items=15`)
      .subscribe((res: any) => {
        if (res != null && res != undefined) {
          if (res.isSuccess == true) {
            console.log('class Res', res);
            if (res.data.length > 0) {
              for (var i = 0; i < res.data.length; i++) {

                this.photos.push({
                  id: res.data[i]._id,
                  url: res.data[i].image.url.replace("http://18.140.57.231", "https://redskyatech.com")

                })
              }
              this.photos.sort(function (a, b) {
                return b.id - a.id
              })


            } else {
              this.isLoadMore = false;
            }
          }
        }
      }, error => {
        console.log(error);

      })

  }

  newChanges() {
    this.pageNo++;
    this.getImages();
  }

  getConfig() {
    var randomTime = [3000,4000,5000,6000,7000,8000]
    var customOptions: OwlOptions = {
      loop: true,
      mouseDrag: false,
      touchDrag: false,
      pullDrag: false,
      dots: false,
      autoplay: true,
      autoplayTimeout: randomTime[Math.floor(Math.random() * 6)],
      autoplayHoverPause: false,
      responsive: {
        0: {
          items: 1
        },
        400: {
          items: 1
        },
        740: {
          items: 1
        },
        1001: {
          items: 1
        }
      },
      nav: false
    }
    return customOptions;
  }


  deleteImg(Img) {
    alert(`delete`)
    this.http.delete(`http://18.140.57.231:6300/api/files/delete/${Img.id}`)
      .subscribe((res: any) => {
        if (res != null && res != undefined) {
          if (res.isSuccess == true) {
            
          }
        }
      },error=>{

      })

  }


  openGallery(index: number = 0) {
    this.ngxImageGallery.open(index);
  }
    
  // close gallery
  closeGallery() {
    this.ngxImageGallery.close();
  }

  galleryOpened(index) {
    console.info('Gallery opened at index ', index);
  }
 
  // callback on gallery closed
  galleryClosed() {
    console.info('Gallery closed.');
  }
 
  // callback on gallery image clicked
  galleryImageClicked(index) {
    console.info('Gallery image clicked with index ', index);
  }
}










