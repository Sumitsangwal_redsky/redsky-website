
import { Component, ElementRef, HostListener, OnDestroy, OnInit } from '@angular/core';
import { trigger, transition, animate, style, state } from '@angular/animations';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Router, NavigationExtras } from '@angular/router';
import * as $ from 'jquery';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('scrollAnimationright', [
      state('show', style({
        opacity: 1,
        transform: "translateX(0)"
      })),
      state('hide', style({
        opacity: 0,
        transform: "translateX(-5%)"
      })),
      transition('show => hide', animate('900ms ease-out')),
      transition('hide => show', animate('900ms ease-in'))
    ]),
    trigger('scrollAnimationleft', [
      state('show', style({
        opacity: 1,
        transform: "translateX(0)"
      })),
      state('hide', style({
        opacity: 0,
        transform: "translateX(+5%)"
      })),
      transition('show => hide', animate('900ms ease-out')),
      transition('hide => show', animate('900ms ease-in'))
    ]),

    trigger('scrollAnimationdown', [
      state('show', style({
        opacity: 1,
        transform: "translateY(0)"
      })),
      state('hide', style({
        opacity: 0,
        transform: "translateY(+15%)"
      })),
      transition('show => hide', animate('900ms ease-out')),
      transition('hide => show', animate('900ms ease-in'))
    ]),

  ]


})
export class HomeComponent implements OnInit, OnDestroy {

  activeSliderCard = 1;
  removerclass = 0;
  incSliderCard: number;
  sliderCardOver = false;
  sliderInterval;
  state = 'hide'
  statehwd = 'hide'
  stateT = 'hide'

  constructor(private router: Router, public el: ElementRef) {}


  ngOnInit() {
    window.scroll(0, 0);

    this.sliderInterval = setInterval(() => {

      this.openBox();

    }, 2000);

    this.checkScroll();

  }

  ngOnDestroy() {
    clearInterval(this.sliderInterval)
  }


  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 2
      },
      1001: {
        items: 4
      }
    },
    nav: false
  }

  customOptions2: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    navSpeed: 700,
    navText: ['<', '>'],
    responsive: {
      0: {
        items: 1
      },
      200: {
        items: 2
      },
      400: {
        items: 2
      },
      800: {
        items: 3
      },
      1001: {
        items: 3
      }
    },
    nav: false
  }


  enter(event) {
    if (document.getElementById(event.target.id).classList.contains("howwedosliderbox1-active")) {


      this.sliderCardOver = true;
    }
  }
  leave() {
    this.sliderCardOver = false;
  }

 @HostListener('window:scroll', ['$event'])
  checkScroll() {
    const componentPosition = this.el.nativeElement.offsetTop;
    const scrollPosition = window.pageYOffset;

    if (scrollPosition >= componentPosition) {
      this.state = 'show'
    } else {
      this.state = 'hide'
    }
    if (scrollPosition >= 800) {
      this.statehwd = 'show'

    } else {
      this.statehwd = 'hide'
    }

    if (scrollPosition >= 1400) {
      this.stateT = 'show'

    } else {
      this.stateT = 'hide'
    }
  }

  click() {
    $("a").on('click', function (event) {
      if (this.hash !== "") {
        event.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 1000, function () {
          window.location.hash = hash;
        });
      }
    });
  };

  navroute(value) {
    this.router.navigate([value]);
    clearInterval(this.sliderInterval);
  }
  sliderroute(value, id) {
    var extras: NavigationExtras = {
      queryParams: {
        "id": id
      }
    }
    this.router.navigate([value], extras);
    clearInterval(this.sliderInterval);
  }

  navsubroute(value, id) {
    clearInterval(this.sliderInterval);
    this.router.navigate([value, id]);
  }

  // .................. Fuctions for how We do It....................

  openBox() {

    if (!this.sliderCardOver) {

      this.removerclass = this.activeSliderCard;

      for (let i = 1; i < 8; i++) {
        document.getElementById("ver-line" + i).classList.remove("linenone");
        document.getElementById("hor-r-" + i).classList.remove("linehide");
        document.getElementById("hor-l-" + i).classList.remove("linehide");
        document.getElementById("cardtop" + i).classList.remove("lineblock");
        document.getElementById("slidercardname" + i).classList.remove("linenone");
        document.getElementById("circle" + i).classList.remove("slidercirculeafter");

        document.getElementById("cardname" + i).classList.remove("lineblock");
        document.getElementById("cardtop" + i).classList.remove("lineblock");
        document.getElementById("slidercardname" + i).classList.remove("linenone");

        document.getElementById("circle" + i).classList.remove("slidercircule2after");
        document.getElementById("cardname" + i).classList.remove("lineblock");


        switch (i) {
          case 1:
            document.getElementById("howwedosliderbox1").classList.remove("howwedosliderbox1-active");
            document.getElementById("circle" + i).classList.add("slidercircule");
            document.getElementById("cardp1").classList.remove("lineblock");
            break;
          case 2:
            document.getElementById("howwedosliderbox2").classList.remove("howwedosliderbox1-active");
            document.getElementById("circle" + i).classList.add("slidercircule2");
            document.getElementById("forgap" + i).classList.remove("lineblock");
            document.getElementById("for2gap2").classList.remove("linenone");
            break;
          case 3:
            document.getElementById("howwedosliderbox3").classList.remove("howwedosliderbox1-active");
            document.getElementById("circle" + i).classList.add("slidercircule");
            document.getElementById("cardp3").classList.remove("lineblock");
            break;
          case 4:
            document.getElementById("howwedosliderbox4").classList.remove("howwedosliderbox1-active");
            document.getElementById("circle" + i).classList.add("slidercircule2");
            document.getElementById("forgap" + i).classList.remove("lineblock");
            document.getElementById("for2gap4").classList.remove("linenone");
            break;
          case 5:
            document.getElementById("howwedosliderbox5").classList.remove("howwedosliderbox1-active");
            document.getElementById("circle" + i).classList.add("slidercircule");
            document.getElementById("cardp5").classList.remove("lineblock");
            break;
          case 6:
            document.getElementById("howwedosliderbox6").classList.remove("howwedosliderbox1-active");
            document.getElementById("circle" + i).classList.add("slidercircule2");
            document.getElementById("forgap" + i).classList.remove("lineblock");
            document.getElementById("for2gap6").classList.remove("linenone");
            break;

          case 7:
            document.getElementById("howwedosliderbox7").classList.remove("howwedosliderbox1-active");
            document.getElementById("circle" + i).classList.add("slidercircule");
            document.getElementById("cardp7").classList.remove("lineblock");
            break;

          default:
            break;
        }
      }


      switch (this.activeSliderCard) {
        case 1:
          document.getElementById("howwedosliderbox1").classList.add("howwedosliderbox1-active");
          document.getElementById("ver-line1").classList.add("linenone");
          document.getElementById("hor-r-1").classList.add("linehide");
          document.getElementById("hor-l-1").classList.add("linehide");
          document.getElementById("hor-l-2").classList.add("linehide");
          document.getElementById("cardtop1").classList.add("lineblock");
          document.getElementById("slidercardname1").classList.add("linenone");
          document.getElementById("circle1").classList.remove("slidercircule");
          document.getElementById("circle1").classList.add("slidercirculeafter");
          document.getElementById("cardname1").classList.add("lineblock");
          document.getElementById("cardp1").classList.add("lineblock");

          this.activeSliderCard++;

          break;
        case 2:
          document.getElementById("howwedosliderbox2").classList.add("howwedosliderbox1-active");
          document.getElementById("ver-line2").classList.add("linenone");
          document.getElementById("hor-r-2").classList.add("linehide");
          document.getElementById("hor-l-2").classList.add("linehide");
          document.getElementById("hor-l-3").classList.add("linehide");
          document.getElementById("hor-r-1").classList.add("linehide");
          document.getElementById("cardtop2").classList.add("lineblock");
          document.getElementById("slidercardname2").classList.add("linenone");
          document.getElementById("circle2").classList.remove("slidercircule2");
          document.getElementById("circle2").classList.add("slidercircule2after");
          document.getElementById("cardname2").classList.add("lineblock");
          document.getElementById("forgap2").classList.add("lineblock");
          document.getElementById("for2gap2").classList.add("linenone");

          this.activeSliderCard++;
          break;

        case 3:
          document.getElementById("howwedosliderbox3").classList.add("howwedosliderbox1-active");
          document.getElementById("ver-line3").classList.add("linenone");
          document.getElementById("hor-r-3").classList.add("linehide");
          document.getElementById("hor-l-3").classList.add("linehide");
          document.getElementById("hor-l-4").classList.add("linehide");
          document.getElementById("hor-r-2").classList.add("linehide");
          document.getElementById("cardtop3").classList.add("lineblock");
          document.getElementById("slidercardname3").classList.add("linenone");
          document.getElementById("circle3").classList.remove("slidercircule");
          document.getElementById("circle3").classList.add("slidercirculeafter");
          document.getElementById("cardname3").classList.add("lineblock");
          document.getElementById("cardp3").classList.add("lineblock");
          this.activeSliderCard++;

          break;
        case 4:
          document.getElementById("howwedosliderbox4").classList.add("howwedosliderbox1-active");
          document.getElementById("ver-line4").classList.add("linenone");
          document.getElementById("hor-r-4").classList.add("linehide");
          document.getElementById("hor-l-4").classList.add("linehide");
          document.getElementById("hor-r-3").classList.add("linehide");
          document.getElementById("hor-l-5").classList.add("linehide");
          document.getElementById("cardtop4").classList.add("lineblock");
          document.getElementById("slidercardname4").classList.add("linenone");
          document.getElementById("circle4").classList.remove("slidercircule2");
          document.getElementById("circle4").classList.add("slidercircule2after");
          document.getElementById("cardname4").classList.add("lineblock");
          document.getElementById("forgap4").classList.add("lineblock");
          document.getElementById("for2gap4").classList.add("linenone");
          this.activeSliderCard++;

          break;
        case 5:
          document.getElementById("howwedosliderbox5").classList.add("howwedosliderbox1-active");
          document.getElementById("ver-line5").classList.add("linenone");
          document.getElementById("hor-r-5").classList.add("linehide");
          document.getElementById("hor-l-5").classList.add("linehide");
          document.getElementById("hor-l-6").classList.add("linehide");
          document.getElementById("hor-r-4").classList.add("linehide");
          document.getElementById("cardtop5").classList.add("lineblock");
          document.getElementById("slidercardname5").classList.add("linenone");
          document.getElementById("circle5").classList.remove("slidercircule");
          document.getElementById("circle5").classList.add("slidercirculeafter");
          document.getElementById("cardname5").classList.add("lineblock");
          document.getElementById("cardp5").classList.add("lineblock");
          this.activeSliderCard++;

          break;
        case 6:
          document.getElementById("howwedosliderbox6").classList.add("howwedosliderbox1-active");
          document.getElementById("ver-line6").classList.add("linenone");
          document.getElementById("hor-r-6").classList.add("linehide");
          document.getElementById("hor-l-6").classList.add("linehide");
          document.getElementById("hor-l-7").classList.add("linehide");
          document.getElementById("hor-r-5").classList.add("linehide");
          document.getElementById("cardtop6").classList.add("lineblock");
          document.getElementById("slidercardname6").classList.add("linenone");
          document.getElementById("circle6").classList.remove("slidercircule2");
          document.getElementById("circle6").classList.add("slidercircule2after");
          document.getElementById("cardname6").classList.add("lineblock");
          document.getElementById("forgap6").classList.add("lineblock");
          document.getElementById("for2gap6").classList.add("linenone");
          this.activeSliderCard++;

          break;

        case 7:
          document.getElementById("howwedosliderbox7").classList.add("howwedosliderbox1-active");
          document.getElementById("ver-line7").classList.add("linenone");
          document.getElementById("hor-r-7").classList.add("linehide");
          document.getElementById("hor-l-7").classList.add("linehide");
          document.getElementById("hor-r-6").classList.add("linehide");
          document.getElementById("cardtop7").classList.add("lineblock");
          document.getElementById("slidercardname7").classList.add("linenone");
          document.getElementById("circle7").classList.remove("slidercircule");
          document.getElementById("circle7").classList.add("slidercirculeafter");
          document.getElementById("cardname7").classList.add("lineblock");
          document.getElementById("cardp7").classList.add("lineblock");
          this.activeSliderCard = 1;

          break;

        default:
          break;
      }


    }

  }

  closeBox() {
    document.getElementById("box").style.display = "none";
  }

}



