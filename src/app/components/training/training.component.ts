import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit {

  traningWebDev;
  traningData;
  constructor() { }

  ngOnInit() {
    window.scroll(0,0);

    this.traningData = [
      {
        id: '0', title: 'NODE JS', des: 'As an asynchronous event-driven JavaScript runtime, Node.js is designed to build scalable network applications.',
         img: '../../../assets/images/nodejsnew.png'
      },
      {
        id: '1', title: 'REACT NATIVE', des:'React Native combines the best parts of native development with React, a best-in-class JavaScript library for building user interfaces.',
         img: '../../../assets/images/whatwedo/reactnative_logo.png'
      },
      {
        id: '2', title: 'PLANE/JAVA SCRIPT', img: '../../../assets/images/whatwedo/reactnative_logo.png'
      },
      {
        id: '3', title: 'ANGULAR TRAINING', des:'For web, mobile web, native mobile and native desktop. Angular code can be written  with HTML, CSS, TypeScript',
         img: '../../../assets/images/whatwedo/angular_logo.png'
      },
      {
        id: '4', title: 'NATIVE SCRIPT',  des:'Open source framework for building truly native mobile apps with Angular, Vue.js, TypeScript, or JavaScript.',
         img: '../../../assets/images/whatwedo/nativescript_logo.png'
      },

      // {
      //   id: '5', title: 'HYBRID APP DEVELOPMENT', img: '../../../assets/images/whatwedo/angular_logo.png'
      // }
      // ,{
      //   id: '0', title: 'BOOTSTRAP TRAINING', img: '../../../assets/images/bootstarp.jpg'
      // },
      {
        id: '5', title: 'HTML TRAINING', img: '../../../assets/images/html.png'
      },

      {
        id: '6', title: 'CSS TRAINING', img: '../../../assets/images/css.jpg'
      }
    ];
  }

}
