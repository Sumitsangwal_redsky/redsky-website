import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TechnologyRoutingModule } from './technology-routing.module';
import { TechnologiesComponent } from './technology/technologies.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    TechnologiesComponent
  ],
  imports: [
    CommonModule,
    TechnologyRoutingModule,
    SharedModule,
  ]





  
})
export class TechnologyModule { }
