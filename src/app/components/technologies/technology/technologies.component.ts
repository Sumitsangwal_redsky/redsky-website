import { Component, OnInit, ElementRef, HostListener } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { HttpClient, } from '@angular/common/http';
@Component({
  selector: "app-technologies",
  templateUrl: "./technologies.component.html",
  styleUrls: ["./technologies.component.css"],
})
export class TechnologiesComponent implements OnInit {
  // nativeScriptVue: any;
  // nativeprojects: any;
  // reactprojects: any;
  // angularprojects: any;
  nativescriptAngularprojects: []
  nativescriptVueprojects: []
  angularprojects: []
  reactNativeprojects: []
  pageId: number;
  isProjectTitle: boolean = false;
  isResizeTitle: boolean = true;
  constructor(private elementRef: ElementRef, private route: ActivatedRoute, private http: HttpClient) { }
  @HostListener("window:resize", ["$event"])
  onresize(event: any) {
    const width = window.innerWidth;
    if (width < 1000) {
      this.isProjectTitle = true;
      this.isResizeTitle = false;
    }
    if (width > 1000) {
      this.isProjectTitle = false;
      this.isResizeTitle = true;
    }
  }
  ngOnInit() {
    this.getProjets();
    this.onresize({});
    window.scroll(0, 0);
    this.route.queryParams.subscribe((data: any) => {
      this.pageId = parseInt(data.id);
    });
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor =
      "linear-gradient(to right, #BD7FC9, #20336D)";
    //   this.nativeprojects = [
    //     {
    //       id: 0,
    //       title: "FARMER'S HUT",
    //       functionality: [
    //         {
    //           id: "Admin can create different categories and can add products.",
    //         },
    //         { id: "Admin can accept or reject an order request" },
    //         { id: "Admin can track to the location of customer for delivery" },
    //         { id: "Customer can add products into card" },
    //         { id: "Customer can check order history" },
    //         { id: "Customer can view the status of current order" },
    //         { id: "Customer can leave feedback for a particular order" },
    //       ],
    //       description:
    //         "Supermarket is now just a click away. With farmer's hut app you can buy all your household needs including - Grocery, household items, personal care products - with touch of your fingertips.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/farmerhuts.gif",
    //     },

    //     {
    //       id: 1,
    //       title: "BMSSS SCHOOL APP",
    //       functionality: [
    //         {
    //           id: "Students and teachers can login with provided credentials.",
    //         },
    //         { id: "Teacher can take students attendances" },
    //         { id: "Students can view their monthly attendance" },
    //         { id: "Students can apply for leave" },
    //         { id: "Admin can update students fee statuses" },
    //         {
    //           id: "Teacher can create school events and upload photos in them",
    //         },
    //       ],
    //       description:
    //         "A great application for students, teachers and admin. With  this app a lot of things can be maintained regarding fees, attendance, holidays and much more.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/school.gif",
    //     },
    //     {
    //       id: 2,
    //       title: "SABG APP",
    //       functionality: [
    //         { id: "Admin can add different categories and can add products" },
    //         {
    //           id:
    //             "Admin can track to customer delivery location for order delivery",
    //         },
    //         { id: "Admin can put offers, discounts on different products" },
    //         { id: "Admin can put offer banners" },
    //         {
    //           id:
    //             "Customer can put items into cart from different categories & can place order",
    //         },
    //         {
    //           id:
    //             "Customer can change/update location for current order during order placement",
    //         },
    //       ],
    //       description:
    //         "Busy with your works at home wanna buy some fresh vegetables or fruits ?  SAB-G APP is the perfect solution for you. The vegetable market is now just one click away.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/subg.gif",
    //     },
    //     {
    //       id: 3,
    //       title: "MUSIC APP",
    //       functionality: [
    //         { id: "User can login with facebook or email" },
    //         { id: "User can view and play latest jams" },
    //         { id: "User can view and play most listened jams" },
    //         { id: "Find jams from diffrent categories." },
    //         {
    //           id:
    //             "Liked a jam ? , You can favourite a jam and  can later listen to it from favourite tab.",
    //         },
    //         { id: "User can comments on a jam & can also see other comments" },
    //         { id: "User can search any jam from all available jams" },
    //       ],
    //       description:
    //         "Big fan of Music - jams ? Music streaming app would be right choice. Managing , listening & sharing jams has become easy with this app.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/music.gif",
    //     },
    //     {
    //       id: 4,
    //       title: "VALET APP",
    //       functionality: [
    //         { id: "Valet can login with fingerprint or email" },
    //         { id: "Valet can view different booking with their details" },
    //         { id: "Valet can change tanent" },
    //         {
    //           id: "Valet can create damages and can upload images of damages",
    //         },
    //         {
    //           id:
    //             "Valet can get customers signature or signature images for check-in/out",
    //         },
    //       ],
    //       description:
    //         "VALET - App is a complete solution for managing bookings, cheak-in/out and different cars service at airports,malls,hotels,restaurants,hospitals & more.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/valet.gif",
    //     },

    //     {
    //       id: 5,
    //       title: "INVENTORY MANAGEMENT APP",
    //       functionality: [
    //         { id: "Seller can add different products in into categories." },
    //         { id: "Seller can generate QR/Barcode for each product." },
    //         { id: "Seller can get the items on a shelf / warehouse / block." },
    //         { id: "Seller can manage stock transactions." },
    //         { id: "Seller can create receipts." },
    //         { id: "Remove inventory as it has been shipped / delivered." },
    //         { id: "Locate current stock." },
    //       ],
    //       description:
    //         "Inventory - Management - App is a solution for seller problems. Seller can manage  his stock, and do different operations on stock available.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/invapp.gif",
    //     },
    //     {
    //       id: 6,
    //       title: "ICECAST STREAMING APP",
    //       functionality: [
    //         { id: "Get all the songs together" },
    //         { id: "Choose any songs for details." },
    //         { id: "Can play  the song without downloading" },
    //         {
    //           id:
    //             " Admin can upload music at icecast server and can create links",
    //         },
    //         { id: "Admin provides data to user for listening" },
    //       ],
    //       description:
    //         "Ice - cast - streaming app lets you to create your own playlist and play whenever you like without downloading any song.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/music2.gif",
    //     },
    //     {
    //       id: 7,
    //       title: "KARD APP",
    //       functionality: [
    //         { id: "User click front image to shown on card" },
    //         { id: "User enter the name of card holder" },
    //         { id: "User can select card background and text colors" },
    //         { id: "User enter delivery details for card" },
    //         { id: "User pay for the service & place order" },
    //       ],
    //       description:
    //         "Kardz4Sportz lets you to create your own card format for ID - Cards of different sports",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/cardsports.gif",
    //     },
    //     {
    //       id: 8,
    //       title: "INYOURFACE APP",
    //       functionality: [
    //         { id: "User logs into the app with email & password." },
    //         { id: "Read different available chapters." },
    //         {
    //           id: "Search any word from any chapter and navigate to that result",
    //         },
    //         { id: "Bookmark any position of chapter" },
    //         { id: "Update profile details." },
    //         { id: "Take quiz of chapter and get the results" },
    //       ],
    //       description:
    //         " Interested in reading people expressions while interaction with them ? This application is the right choice for you.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/inyourface.gif",
    //     },

    //     {
    //       id: 9,
    //       title: "LENDI APPLICATION",
    //       functionality: [
    //         { id: "Login/Register with email and password." },
    //         { id: "See the items sellers have posted." },
    //         {
    //           id:
    //             "View the item details such as hourly rate, rating, dimensions etc.",
    //         },
    //         { id: "Request the item if need." },
    //         { id: "Upload an item that you want to be lent." },
    //         { id: "View the requests for previously uploaded items." },
    //         {
    //           id:
    //             "Once confirmed chat with buyer/seller with the help of in-built chat feature.",
    //         },
    //       ],
    //       description:
    //         "Lendi connects buyer and seller with each other to help both.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/lendii.gif",
    //     },
    //     {
    //       id: 10,
    //       title: "MUSIC Q",
    //       functionality: [
    //         {
    //           id:
    //             "Keep yourself updated with latest albums and latest playlists.",
    //         },
    //         {
    //           id:
    //             "Listen to most engaged songs with 'Most liked' and 'Most viewed' lists.",
    //         },
    //         { id: "Never lose your rhythm with 'Recently Played' songs." },
    //         { id: "Create your own playlists as per your music tastes." },
    //         {
    //           id:
    //             "Be aware of cool events around you with the help of 'Events and Announcements'.",
    //         },
    //         {
    //           id: "Listen to your own downloaded songs under 'Offline' section.",
    //         },
    //       ],
    //       description:
    //         "Answer to all your music problems. A great solution to listen to good music and manage it.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/music_solutation.gif",
    //     },
    //     {
    //       id: 11,
    //       title: "MR. AND MRS APPLICATION",
    //       functionality: [
    //         { id: "Easy to find love in your area." },
    //         { id: "Add preferences for your ideal person." },
    //         {
    //           id:
    //             "Someone looks good for you? Get to know about them better by looking into their profile's 'About' and 'Interests' sections.",
    //         },
    //         {
    //           id:
    //             "Make something special out of your pictures with in-built filters.",
    //         },
    //         {
    //           id:
    //             "Don't know how to start? Just don't worry and get help from 'Doctor love'",
    //         },
    //         {
    //           id:
    //             "Get to them in real sense with the help of our audio and video call features.",
    //         },
    //         {
    //           id:
    //             "Keep yourself updated regarding events and announcements in your area.",
    //         },
    //         {
    //           id:
    //             "Someone looks suspicious that information is false, block them or report them. The admin will handle it in best way for you.",
    //         },
    //         { id: "Opt another pack of cool features with subscriptions." },
    //       ],
    //       description:
    //         "Find love in snap of fingers with MrAndMrs application. It also gives information of different events and announcements around you to catch up.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/mrandmrs.gif",
    //     },
    //     {
    //       id: 12,
    //       title: "SS BROKERS",
    //       functionality: [
    //         { id: "Login as buyer or seller." },
    //         { id: "As Admin upload your bulk stock with quantity." },
    //         { id: "As Admin get buyer's queries and contact to resolve." },
    //         { id: "See the previous queries by buyers." },
    //         { id: "Sort the queries as per filters." },
    //         { id: "Get queries details of any date on email anytime." },
    //         { id: "As a buyer, view stocks on the dashboard." },
    //         {
    //           id:
    //             "Curious about some stock? Put a query to the Seller and get it resolved.",
    //         },
    //       ],
    //       description:
    //         "A new way to manage your inventory. Buy or sell the bulk stock with ease.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/ssbroker.gif",
    //     },
    //   ];

    //   this.nativeScriptVue = [
    //     {
    //       id: 1,
    //       title: "BAZEBO APP",
    //       functionality: [
    //         { id: "Add money to wallet to continue seamless shopping." },
    //         { id: "Add items to bazebo-box with friends collectively." },
    //         {
    //           id:
    //             "Didn't find any item? Put a request for the item and it will be available in no time.",
    //         },
    //         { id: "Get current or previous order status anytime anywhere." },
    //         { id: "A lot of categories to choose from." },
    //         {
    //           id:
    //             "Not sure whether to buy right now? Add to wishlist for shopping in future.",
    //         },
    //         {
    //           id:
    //             "Check whether an item is right for you or not from feedback and reviews about the item.",
    //         },
    //         {
    //           id:
    //             "Different sizes available for different items so that you can choose a perfect item for you.",
    //         },
    //       ],
    //       description:
    //         "Shopping online has never been this easy. It is in your hands with Bazebo.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/bazebo.gif",
    //     },
    //     {
    //       id: 2,
    //       title: "CLENZAIR LOCATION APP",
    //       functionality: [
    //         { id: "Find the nearby restaurants." },
    //         {
    //           id:
    //             "See the distance between the selected restaurant and your current location.",
    //         },
    //         { id: "Filter the location as per available filters." },
    //         { id: "View the location details." },
    //       ],
    //       description:
    //         "Equipped with rich google location apis, clenzair application helps to find nearby restaurants, venues etc.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/clanzair.gif",
    //     },
    //   ];

    //   this.angularprojects = [
    //     {
    //       id: 1,
    //       title: "Poker Web",
    //       functionality: [
    //         {
    //           id:
    //             "Register with your details such as email , address & account information.",
    //         },
    //         { id: "View the guide how to play." },
    //         { id: "View different ongoing tournaments." },
    //         {
    //           id:
    //             "Play poker and keep your game infomation managed in your profile.",
    //         },
    //         { id: "Message chat with your friend added in your friend list." },
    //       ],
    //       description:
    //         "Are you a poker - fan or are you a poker champion ? Play online poker here and win money and cool prices.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/poker.mp4",
    //     },
    //     {
    //       id: 2,
    //       title: "BMSSS ADMIN PANEL",
    //       functionality: [
    //         { id: "Admin can add teachers and students." },
    //         { id: "Admin can update teachers and students profile." },
    //         { id: "Admin can view or search all students and teachers." },
    //       ],
    //       description:
    //         "With BMSSS School Admin panel admin can manage teachers,students data and can execute many more operations.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/bmsss.mp4",
    //     },
    //     {
    //       id: 3,
    //       title: "Music Q Admin Panel",
    //       functionality: [
    //         {
    //           id: "View current active and inactive users of the application.",
    //         },
    //         { id: "Add new 'Events and Announcements'." },
    //         { id: "Manage subscription plans." },
    //         { id: "Add latest playlists and albums." },
    //       ],
    //       description:
    //         "Music Solution Admin Panel manages the application, updates and adds new things.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/music_admin.mp4",
    //     },
    //     {
    //       id: 4,
    //       title: "Mr and Mrs Admin Panel",
    //       functionality: [
    //         { id: "Manage users of the application (MrAndMrs)" },
    //         { id: "Make users active or inactive as per required scenario" },
    //         {
    //           id:
    //             "Add events and announcements for the users to see in the application",
    //         },
    //         { id: "Create different subscriptions for the application users" },
    //       ],
    //       description:
    //         "Mr and Mrs Admin Panel can manage the users as well as other things related to the application.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/mrandmrs_admin.mp4",
    //     },
    //     {
    //       id: 5,
    //       title: "Eat-It Admin Panel",
    //       functionality: [
    //         { id: "Verify different users." },
    //         { id: "Make users active or inactive as per required scenarios." },
    //         {
    //           id:
    //             "Add cuisines with image to make it available in the application.",
    //         },
    //       ],
    //       description:
    //         "Eat-It Admin Panel is for management of the Eat-It application.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/eatit_admin.mp4",
    //     },
    //   ];

    //   this.reactprojects = [
    //     {
    //       id: 0,
    //       title: "DRIVING QUIZ APP",
    //       functionality: [
    //         { id: "Register with your email." },
    //         { id: "Read practice theory or mock theory" },
    //         { id: "Start your driving quiz" },
    //         { id: "View your progress board" },
    //         { id: "View correct answers" },
    //       ],
    //       description:
    //         "Looking for driving licence & want to learn about the traffic guide & rules? Learing how to drive & rules of traffic has never been this easy & fun.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/driving.gif",
    //     },
    //     {
    //       id: 1,
    //       title: "HEATER APP",
    //       functionality: [
    //         {
    //           id:
    //             "You can connect the electronic devices with mobile using bluetooth and server.",
    //         },
    //         { id: "Change the temperature of devices." },
    //         {
    //           id:
    //             "Schedule the time for ON/OFF the devices and also set reminder for any day within the week for ON and OFF the electronic devices.",
    //         },
    //       ],
    //       description:
    //         "Heater app is used to manage the electronic devices (like Air-Conditioner) through mobile.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/Heater_app.gif",
    //     },
    //     {
    //       id: 2,
    //       title: "INVEST CHECK",
    //       functionality: [
    //         {
    //           id:
    //             "InvestCheck comes with an elegant & flexible UI which has been optimised for all screens.",
    //         },
    //         {
    //           id:
    //             "App is also password protected so you can keep safe your daily income and expense transaction detail.",
    //         },
    //         {
    //           id:
    //             "It shows account transactions in the form of table with chart representation.",
    //         },
    //         {
    //           id:
    //             "It shows filters (current year, 3 year) etc.. to check transaction details.",
    //         },
    //         { id: "It evaluates and check performance also." },
    //       ],
    //       description:
    //         "It makes accounting easy to manage.you can manage your all personal , general account easily.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/investcheck.gif",
    //     },
    //     {
    //       id: 3,
    //       title: "Eat-It Application",
    //       functionality: [
    //         { id: "Buy food as a customer or Sell food as seller." },
    //         {
    //           id:
    //             "Get food recommendations as per your location or with filters available.",
    //         },
    //         {
    //           id:
    //             "Do community food distribution with options 'Langar' and 'Free Food'.",
    //         },
    //         { id: "Give feedback about bought food." },
    //         { id: "Manage your profile in 'Profile' section." },
    //         {
    //           id:
    //             "Get to know the foods you bought or sold in 'My food' section.",
    //         },
    //         { id: "Chat with customer or seller after order confirmation." },
    //         {
    //           id: "Contact seller or buyer with different communication methods.",
    //         },
    //         {
    //           id: "Crop the food image as per your requirement while uploading.",
    //         },
    //       ],
    //       description:
    //         "Buy and sell food with Eat-it. Other exciting features are also there which make it different from other apps.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/eatit.gif",
    //     },
    //     {
    //       id: 4,
    //       title: "DATING APP",
    //       functionality: [
    //         { id: "Login with Facebook,Google or with email." },
    //         { id: "Like, super like or left swipe for showing interest " },
    //         { id: "Chat with the who are interested in you" },
    //         { id: "Update your profile,put highlights" },
    //         {
    //           id:
    //             "Get fastest firebase notification and never miss message from our loved ones.",
    //         },
    //       ],
    //       description:
    //         "Single and ready to mingle ? Looping for a serious relationship. Find your soulmate with the help of our Dating App.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/datingapp.gif",
    //     },
    //     {
    //       id: 5,
    //       title: "POINT OF SALE",
    //       functionality: [
    //         { id: "Add a new sale" },
    //         { id: "Get the transaction details" },
    //         { id: "Void the transactions." },
    //         { id: "Manage rejected transaction" },
    //         { id: "Set the price of sales" },
    //       ],
    //       description:
    //         "This application helps you to manage your sale details and put operations on them.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/pointofsale.gif",
    //     },
    //     {
    //       id: 6,
    //       title: "RENTOMACH",
    //       functionality: [
    //         {
    //           id: "Customers can search Machine for Rent and get best price.",
    //         },
    //         {
    //           id:
    //             "You have the power to review and edit all your equipment reservations, schedule a pick up or service call, or extend a rental.",
    //         },
    //         {
    //           id:
    //             "We make your Machinery available in our wide-reaching marketing campaigns.",
    //         },
    //       ],
    //       description: "RentoMach.co is the top Machinery rental place.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/rentomach.gif",
    //     },
    //     {
    //       id: 7,
    //       title: "PROPERTY STRENGTH",
    //       functionality: [
    //         { id: "Login with your mobile number" },
    //         { id: "Register as an agent or a customer" },
    //         { id: "Share data through whatsapp or text message." },
    //         { id: "Put property for sale articles as an agent" },
    //         { id: "View and confirm for buying property as customers" },
    //       ],
    //       description:
    //         "An application to help with  your buying or selling property related issues.",
    //       img: "../../../assets/images/nativ.png",
    //       gif: "../../../assets/images/project/property.gif",
    //     },
    //   ];
  };



  getProjets() {
    
    this.http.get(`https://redskyatech.com/v1/redsky/apis/projects?visibility=website`)
      .subscribe((res: any) => {
        if (res != null && res != undefined) {
          if (res.isSuccess == true) {
            console.log('projects Res', res);
            if (res.data.length > 0) {
              let projects: any = res.data;
              this.nativescriptAngularprojects = projects.filter(data => data.type == "nactiveAngular");
              this.nativescriptVueprojects = projects.filter(data => data.type == "nativeScriptVue");
              this.angularprojects = projects.filter(data => data.type == "angular");
              this.reactNativeprojects = projects.filter(data => data.type == "reactNative");

            }
          }
        }
      }, error => {
        console.log(error);

      })

  }

  onTabChanged(args) {
    window.scroll(0, 0)
  }
}
