import { Component, OnInit, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { ThankuComponent } from '../thanku/thanku.component';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { NullVisitor } from '@angular/compiler/src/render3/r3_ast';
import { style } from '@angular/animations';

@Component({
  selector: 'app-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.css']
})
export class CareersComponent implements OnInit {
  applyForm: FormGroup;
  careers;
  postId = 0;
  fileData: File;
  firstName: string;
  lastName: string;
  email: string;
  mobileNo: string;
  skills: string;
  collage: string
  formData;
  url = "http://18.139.111.3:6300/api/applyPost";
  progressBar = false;
  showErrors: boolean;

  constructor(private http: HttpClient, public dialog: MatDialog) { }

  ngOnInit() {
    this.showErrors = false;
    this.applyForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      lastname: new FormControl(null, Validators.nullValidator),
      mobile: new FormControl(null, Validators.required),
      email: new FormControl(null, Validators.required),
      qualification: new FormControl(null, Validators.required),
      university: new FormControl(null, Validators.nullValidator),
    })

    this.careers = [
      {
        id: '0', title: 'NODE JS', description: 'Node.js is designed to build scalable network applications. In the following "hello world" example, many connections can be handled concurrently. Upon each connection, the callback is fired, but if there is no work to be done, Node.js will sleep.',
        img: '../../../assets/icons/nodejs_logo.png'
      },
      {
        id: '1', title: 'REACT NATIVE', description: 'React Native is an open-source mobile application framework created by Facebook. It is used to develop applications for Android, iOS, Web and UWP by enabling developers to use React along with native platform capabilities. Furthermore, an incomplete port for Qt also exists.',
        img: '../../../assets/images/whatwedo/reactnative_logo.png'
      },
      {
        id: '2', title: 'JAVA SCRIPT PLANE/(PLANE)', description: "JavaScript, often abbreviated as JS, is a high-level, just-in-time compiled, object-oriented programming language that conforms to the ECMAScript specification. JavaScript has curly-bracket syntax, dynamic typing, prototype-based object-orientation, and first-class functions.",
        img: '../../../assets/icons/javascript_logo.png'
      },
      {
        id: '3', title: 'ANGULAR TRAINING', description: 'Angular is a TypeScript-based open-source web application framework led by the Angular Team at Google and by a community of individuals and corporations. Angular is a complete rewrite from the same team that built AngularJS.',
        img: '../../../assets/icons/angular_logo.png'
      },
      {
        id: '4', title: 'NATIVE SCRIPT', description: 'NativeScript is an open-source framework to develop apps on the Apple iOS and Android platforms. It was originally conceived and developed by Progress. NativeScript apps are built using JavaScript, or by using any language that transpiles to JavaScript, such as TypeScript.',
        img: '../../../assets/images/whatwedo/nativescript_logo.png'
      },
      {
        id: '5', title: 'HTML TRAINING', description: "Hypertext Markup Language is the standard markup language for documents designed to be displayed in a web browser. It can be assisted by technologies such as Cascading Style Sheets and scripting languages such as JavaScript.", img: '../../../assets/icons/html5_logo.png'
      },

      {
        id: '6', title: 'CSS TRAINING', description: "Cascading Style Sheets is a style sheet language used for describing the presentation of a document written in a markup language like HTML. CSS is a cornerstone technology of the World Wide Web, alongside HTML and JavaScript.", img: '../../../assets/icons/css3_logo.png'
      }
    ];
  }

  apply(id) {

    this.postId = id;
    document.getElementById("closebtn").style.display = "block";
    document.getElementById("mask").style.bottom = "0%";

  }

  closemask() {
    document.getElementById("mask").style.bottom = "100%";
    document.getElementById("closebtn").style.display = "none";
  }

  onKey(value, id) {
    switch (id) {
      case 0:
        this.firstName = value.target.value;
        break;

      case 1:
        this.lastName = value.target.value;
        break;

      case 2:
        this.email = value.target.value;
        break;

      case 3:
        this.mobileNo = value.target.value;
        break;

      case 4:
        this.skills = value.target.value;
        break;

      case 5:
        this.collage = value.target.value;
        break;

      default:
        break;
    }
  }

  onFileChange(file: any) {
    this.fileData = <File>file.target.files[0];

  }

  onSubmit(event) {

    if (this.firstName == undefined || this.firstName == "") {
      this.showErrors = true;

    } else if (this.email == undefined || this.email == "") {
      this.showErrors = true;


    } else if (this.mobileNo == undefined || this.mobileNo == "") {
      this.showErrors = true;


    } else if (this.skills == undefined || this.skills == "") {
      this.showErrors = true;


    } else if (this.collage == undefined || this.collage == "") {
      this.showErrors = true;

    }
    else {
      this.progressBar = true;

      let body = {
        firstName: this.firstName,
        lastName: this.lastName,
        email: this.email,
        phone: this.mobileNo,
        qualification: this.skills,
        collage: this.collage
      }
      this.http.post(this.url, body).subscribe((res: any) => {

        if (res != null && res != undefined) {
          if (res.isSuccess) {
            this.progressBar = false
            this.openDialog();
            // this.router.navigate(['thank-you']);
          }

        }

      }, error => {
        console.log(error);
        this.progressBar = false;
        alert(error.error.error);
      });


    }
  }

  postData() {

  }

  openDialog() {
    const dialogRef = this.dialog.open(ThankuComponent, {
      panelClass: 'my-class'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  onProfileFormSubmit() {

  }

}
