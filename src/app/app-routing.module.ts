import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { SliderComponent } from './components/slider/slider.component';
import { CareersComponent } from './components/careers/careers.component';
import { HybridappComponent } from './components/hybridapp/hybridapp.component';
import { BiddingComponent } from './components/bidding/bidding.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { ThankuComponent } from './components/thanku/thanku.component';
import { TrainingComponent } from './components/training/training.component';
import { HiringComponent } from './components/hiring/hiring.component';
import { HoverviewComponent } from './components/hoverview/hoverview.component';


export const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'slider', component: SliderComponent },
  { path: 'careers', component: CareersComponent },
  { path: 'hiring', component: HiringComponent },
  { path: 'technologies', loadChildren: () => import("./components/technologies/technology.module").then(m => m.TechnologyModule) },
  { path: 'hybridapp/:id', component: HybridappComponent },
  { path: 'bidding', component: BiddingComponent },
  { path: 'thank-you', component: ThankuComponent },
  { path: 'training', component: TrainingComponent },
  { path: 'hoverview', component: HoverviewComponent },
  { path: '**', component: PagenotfoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
