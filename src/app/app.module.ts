import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { StickyModule } from 'ng2-sticky-kit';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import { ScrollingModule } from '@angular/cdk/scrolling';
import { SliderModule } from 'angular-image-slider';
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { AboutComponent } from './components/about/about.component';
import { HomeComponent } from './components/home/home.component';
import { SharedModule } from './shared/shared.module';
import { NgImageSliderModule } from 'ng-image-slider';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CarouselModule } from 'ngx-owl-carousel-o';
// import {
//   IgxButtonModule,
//   IgxIconModule,
//   IgxNavigationDrawerModule,
//   IgxRippleModule,
//   IgxToggleModule
// } from "igniteui-angular";
import { SliderComponent } from './components/slider/slider.component';

import { CareersComponent } from './components/careers/careers.component';
import { HttpClientModule } from '@angular/common/http';
// import { TechnologiesComponent } from './components/technologies/technology/technologies.component';
import { HybridappComponent } from './components/hybridapp/hybridapp.component';
import { BiddingComponent } from './components/bidding/bidding.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { ThankuComponent } from './components/thanku/thanku.component';
import { TrainingComponent } from './components/training/training.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HiringComponent } from './components/hiring/hiring.component';
import { HoverviewComponent } from './components/hoverview/hoverview.component';
import { NgxImageGalleryModule } from 'ngx-image-gallery';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    SliderComponent,
    CareersComponent,
    HybridappComponent,
    BiddingComponent,
    PagenotfoundComponent,
    ThankuComponent,
    TrainingComponent,
    HiringComponent,
    HoverviewComponent

  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SliderModule,
    NgImageSliderModule,
    NgbModule,
    FlexLayoutModule,
    CarouselModule,
    HttpClientModule,
    StickyModule,
    SharedModule,
    NgxImageGalleryModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })

  ],

  providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
